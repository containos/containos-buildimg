FROM debian:stable
MAINTAINER Angus Lees <gus@inodes.org>

RUN adduser --disabled-password --home /build --gecos 'CI user' ciuser

VOLUME /downloads
VOLUME /sstate-cache

ENV DL_DIR=/downloads SSTATE_DIR=/sstate-cache LANG=en_US.UTF-8

RUN \
    set -x -e; \
    apt-get -qy update; \
    apt-get -qy install --no-install-recommends \
    gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    python-dev python3-dev \
    xz-utils debianutils iputils-ping file \
    libsdl1.2-dev xterm ca-certificates sysstat iproute2 xvfb locales \
    openssh-client procps; \
    echo en_US.UTF-8 UTF-8 >> /etc/locale.gen; \
    locale-gen; \
    apt-get -qy clean; \
    chown -R ciuser: $DL_DIR $SSTATE_DIR

USER ciuser

RUN \
    set -x -e; \
    git config --global gc.auto 0; \
    git config --global user.name buildimg; \
    git config --global user.email buildimg@containos